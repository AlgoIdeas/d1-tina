#ifndef __homec_minjie_yu_linux_sdk_lichee_brandy_2_0_spl_include_config_h_
#define __homec_minjie_yu_linux_sdk_lichee_brandy_2_0_spl_include_config_h_
#include<sun20iw1p1.h>
#define CFG_ARCH_RISCV 1
#define CFG_AXP1530_POWER 1
#define CFG_BOOT0_RUN_ADDR 0x20000
#define CFG_FES1_RUN_ADDR 0x28000
#define CFG_LICHEE_BOARD CFG_
#define CFG_SBOOT_RUN_ADDR 0x20480
#define CFG_SPINOR_UBOOT_OFFSET 128
#define CFG_SPI_USE_DMA 1
#define CFG_SUNXI_AUTO_TWI 1
#define CFG_SUNXI_CHIPID 1
#define CFG_SUNXI_DMA 1
#define CFG_SUNXI_GPIO_V2 1
#define CFG_SUNXI_GUNZIP 1
#define CFG_SUNXI_LZ4 1
#define CFG_SUNXI_LZMA 1
#define CFG_SUNXI_MEMOP 1
#define CFG_SUNXI_PMIC 1
#define CFG_SUNXI_POWER 1
#define CFG_SUNXI_SPI 1
#define CFG_SUNXI_SPINOR 1
#define CFG_SUNXI_TWI 1
#define CFG_SYS_INIT_RAM_SIZE 0x10000
#define CFG_USE_DCACHE 1
#define CFG_default 1
#define CFG_nezha 2
#define CFG_nezha_min 3
#define PLATFORM_CHOICES d1
#define PLATFORM_LIBGCC -L /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/bin/../lib/gcc/riscv64-unknown-linux-gnu/8.1.0/lib64xthead/lp64d -lgcc
#endif
