cmd_u-boot.lds := ./../tools/toolchain/riscv64-linux-x86_64-20200528/bin/riscv64-unknown-linux-gnu-gcc -E -Wp,-MD,./.u-boot.lds.d -D__KERNEL__ -D__UBOOT__   -ffixed-gp -fpic  -fno-common -gdwarf-2 -ffunction-sections -fdata-sections -pipe -march=rv64imacxthead -mabi=lp64 -mcmodel=medlow -Iinclude   -I./arch/riscv/include -include ./include/linux/kconfig.h -I./include/openssl  -nostdinc -isystem /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/bin/../lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include -ansi -include ./include/u-boot/u-boot.lds.h -DCPUDIR=arch/riscv/cpu/c906  -D__ASSEMBLY__ -x assembler-with-cpp -P -o u-boot.lds arch/riscv/cpu/u-boot.lds

source_u-boot.lds := arch/riscv/cpu/u-boot.lds

deps_u-boot.lds := \
    $(wildcard include/config/sys/text/base.h) \
    $(wildcard include/config/sunxi/text/size.h) \
  include/u-boot/u-boot.lds.h \

u-boot.lds: $(deps_u-boot.lds)

$(deps_u-boot.lds):
