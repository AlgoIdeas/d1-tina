#define PLAIN_VERSION "2018.05-g0a88ac9-dirty-config-dirty"
#define U_BOOT_VERSION "U-Boot " PLAIN_VERSION
#define CC_VERSION_STRING "riscv64-unknown-linux-gnu-gcc (C-SKY RISCV Tools V1.8.3 B20200528) 8.1.0"
#define LD_VERSION_STRING "GNU ld (GNU Binutils) 2.32"
