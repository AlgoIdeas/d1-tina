#ifndef __ASM_OFFSETS_H__
#define __ASM_OFFSETS_H__
/*
 * DO NOT MODIFY.
 *
 * This file was generated by Kbuild
 */

#define GD_BOOT_HART 272 /* offsetof(gd_t, arch.boot_hart)	# */
#define GD_FIRMWARE_FDT_ADDR 280 /* offsetof(gd_t, arch.firmware_fdt_addr)	# */
#define GD_AVAILABLE_HARTS 288 /* offsetof(gd_t, arch.available_harts)	# */

#endif
