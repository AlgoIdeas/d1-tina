cmd_drivers/mtd/ubi/crc32.o := ./../tools/toolchain/riscv64-linux-x86_64-20200528/bin/riscv64-unknown-linux-gnu-gcc -Wp,-MD,drivers/mtd/ubi/.crc32.o.d  -nostdinc -isystem /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/bin/../lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include -Iinclude   -I./arch/riscv/include -include ./include/linux/kconfig.h -I./include/openssl -D__KERNEL__ -D__UBOOT__ -Wall -Wstrict-prototypes -Wno-format-security -fno-builtin -ffreestanding -Werror -Wno-packed-bitfield-compat -fshort-wchar -Os -fno-stack-protector -fno-delete-null-pointer-checks -fmacro-prefix-map=./= -g -fstack-usage -Wno-format-nonliteral -Werror=date-time -ffixed-gp -fpic -fno-common -gdwarf-2 -ffunction-sections -fdata-sections -pipe -march=rv64imacxthead -mabi=lp64 -mcmodel=medlow    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(crc32)"  -D"KBUILD_MODNAME=KBUILD_STR(crc32)" -c -o drivers/mtd/ubi/crc32.o drivers/mtd/ubi/crc32.c

source_drivers/mtd/ubi/crc32.o := drivers/mtd/ubi/crc32.c

deps_drivers/mtd/ubi/crc32.o := \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/use/stdint.h) \
  include/linux/posix_types.h \
  include/linux/stddef.h \
  arch/riscv/include/asm/posix_types.h \
  arch/riscv/include/asm/types.h \
    $(wildcard include/config/arch/rv64i.h) \
  include/asm-generic/int-ll64.h \
  /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include/stddef.h \
  /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include/stdbool.h \
  arch/riscv/include/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/kasan.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/linux/byteorder/swab.h \
  include/linux/byteorder/generic.h \
  drivers/mtd/ubi/crc32defs.h \
  drivers/mtd/ubi/crc32table.h \

drivers/mtd/ubi/crc32.o: $(deps_drivers/mtd/ubi/crc32.o)

$(deps_drivers/mtd/ubi/crc32.o):
