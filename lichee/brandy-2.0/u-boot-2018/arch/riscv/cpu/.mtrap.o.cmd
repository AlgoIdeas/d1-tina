cmd_arch/riscv/cpu/mtrap.o := ./../tools/toolchain/riscv64-linux-x86_64-20200528/bin/riscv64-unknown-linux-gnu-gcc -Wp,-MD,arch/riscv/cpu/.mtrap.o.d  -nostdinc -isystem /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/bin/../lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include -Iinclude   -I./arch/riscv/include -include ./include/linux/kconfig.h -I./include/openssl -D__KERNEL__ -D__UBOOT__ -D__ASSEMBLY__ -g -ffixed-gp -fpic -fno-common -gdwarf-2 -ffunction-sections -fdata-sections -pipe -march=rv64imacxthead -mabi=lp64 -mcmodel=medlow   -c -o arch/riscv/cpu/mtrap.o arch/riscv/cpu/mtrap.S

source_arch/riscv/cpu/mtrap.o := arch/riscv/cpu/mtrap.S

deps_arch/riscv/cpu/mtrap.o := \
    $(wildcard include/config/32bit.h) \
    $(wildcard include/config/riscv/mmode.h) \
  include/common.h \
    $(wildcard include/config/sys/support/64bit/data.h) \
    $(wildcard include/config/env/is/embedded.h) \
    $(wildcard include/config/sys/malloc/len.h) \
    $(wildcard include/config/env/addr.h) \
    $(wildcard include/config/env/size.h) \
    $(wildcard include/config/sys/monitor/base.h) \
    $(wildcard include/config/sys/monitor/len.h) \
    $(wildcard include/config/env/is/in/nvram.h) \
    $(wildcard include/config/auto/complete.h) \
    $(wildcard include/config/cmd/eeprom.h) \
    $(wildcard include/config/env/eeprom/is/on/i2c.h) \
    $(wildcard include/config/sys/i2c/eeprom/addr.h) \
    $(wildcard include/config/sys/def/eeprom/addr.h) \
    $(wildcard include/config/mpc8xx/spi.h) \
    $(wildcard include/config/sys/dram/test.h) \
    $(wildcard include/config/arm.h) \
    $(wildcard include/config/led/status.h) \
    $(wildcard include/config/show/activity.h) \
    $(wildcard include/config/mp.h) \
    $(wildcard include/config/post.h) \
    $(wildcard include/config/has/post.h) \
    $(wildcard include/config/post/alt/list.h) \
    $(wildcard include/config/post/std/list.h) \
    $(wildcard include/config/init/critical.h) \
    $(wildcard include/config/skip/lowlevel/init.h) \
    $(wildcard include/config/efi/stub.h) \
  arch/riscv/include/asm/encoding.h \
    $(wildcard include/config/sunxi/riscv/mode.h) \
    $(wildcard include/config/riscv/smode.h) \
    $(wildcard include/config/string/addr.h) \
    $(wildcard include/config/64bit.h) \
  arch/riscv/include/asm/csr.h \
  arch/riscv/include/asm/asm.h \

arch/riscv/cpu/mtrap.o: $(deps_arch/riscv/cpu/mtrap.o)

$(deps_arch/riscv/cpu/mtrap.o):
