cmd_arch/riscv/dts/sun20iw1p1-soc-system.dtb := mkdir -p arch/riscv/dts/ ; (cat arch/riscv/dts/sun20iw1p1-soc-system.dts; ) > arch/riscv/dts/.sun20iw1p1-soc-system.dtb.pre.tmp; ./../tools/toolchain/riscv64-linux-x86_64-20200528/bin/riscv64-unknown-linux-gnu-gcc -E -Wp,-MD,arch/riscv/dts/.sun20iw1p1-soc-system.dtb.d.pre.tmp -nostdinc -I./arch/riscv/dts -I./arch/riscv/dts/include -Iinclude -I./include -I./arch/riscv/include -include ./include/linux/kconfig.h -D__ASSEMBLY__ -undef -D__DTS__ -x assembler-with-cpp -o arch/riscv/dts/.sun20iw1p1-soc-system.dtb.dts.tmp arch/riscv/dts/.sun20iw1p1-soc-system.dtb.pre.tmp ; ./scripts/dtc/dtc -O dtb -o arch/riscv/dts/sun20iw1p1-soc-system.dtb -b 0 -i arch/riscv/dts/  -Wno-unit_address_vs_reg -Wno-simple_bus_reg -Wno-unit_address_format -Wno-pci_bridge -Wno-pci_device_bus_num -Wno-pci_device_reg -R 4 -p 0x1000 -d arch/riscv/dts/.sun20iw1p1-soc-system.dtb.d.dtc.tmp arch/riscv/dts/.sun20iw1p1-soc-system.dtb.dts.tmp ; cat arch/riscv/dts/.sun20iw1p1-soc-system.dtb.d.pre.tmp arch/riscv/dts/.sun20iw1p1-soc-system.dtb.d.dtc.tmp > arch/riscv/dts/.sun20iw1p1-soc-system.dtb.d

source_arch/riscv/dts/sun20iw1p1-soc-system.dtb := arch/riscv/dts/.sun20iw1p1-soc-system.dtb.pre.tmp

deps_arch/riscv/dts/sun20iw1p1-soc-system.dtb := \
  include/dt-bindings/interrupt-controller/arm-gic.h \
  include/dt-bindings/interrupt-controller/irq.h \
  include/dt-bindings/gpio/gpio.h \
  arch/riscv/dts/sun20iw1p1-clk.dtsi \
  include/dt-bindings/thermal/thermal.h \
  arch/riscv/dts/.board-uboot.dts \

arch/riscv/dts/sun20iw1p1-soc-system.dtb: $(deps_arch/riscv/dts/sun20iw1p1-soc-system.dtb)

$(deps_arch/riscv/dts/sun20iw1p1-soc-system.dtb):
