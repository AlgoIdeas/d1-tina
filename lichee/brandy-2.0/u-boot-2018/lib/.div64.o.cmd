cmd_lib/div64.o := ./../tools/toolchain/riscv64-linux-x86_64-20200528/bin/riscv64-unknown-linux-gnu-gcc -Wp,-MD,lib/.div64.o.d  -nostdinc -isystem /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/bin/../lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include -Iinclude   -I./arch/riscv/include -include ./include/linux/kconfig.h -I./include/openssl -D__KERNEL__ -D__UBOOT__ -Wall -Wstrict-prototypes -Wno-format-security -fno-builtin -ffreestanding -Werror -Wno-packed-bitfield-compat -fshort-wchar -Os -fno-stack-protector -fno-delete-null-pointer-checks -fmacro-prefix-map=./= -g -fstack-usage -Wno-format-nonliteral -Werror=date-time -ffixed-gp -fpic -fno-common -gdwarf-2 -ffunction-sections -fdata-sections -pipe -march=rv64imacxthead -mabi=lp64 -mcmodel=medlow    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(div64)"  -D"KBUILD_MODNAME=KBUILD_STR(div64)" -c -o lib/div64.o lib/div64.c

source_lib/div64.o := lib/div64.c

deps_lib/div64.o := \
  include/linux/compat.h \
    $(wildcard include/config/lbdaf.h) \
  include/malloc.h \
    $(wildcard include/config/sys/malloc/simple.h) \
  include/linux/stddef.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/use/stdint.h) \
  include/linux/posix_types.h \
  arch/riscv/include/asm/posix_types.h \
  arch/riscv/include/asm/types.h \
    $(wildcard include/config/arch/rv64i.h) \
  include/asm-generic/int-ll64.h \
  /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include/stddef.h \
  /homec/minjie.yu/linux/sdk/lichee/brandy-2.0/tools/toolchain/riscv64-linux-x86_64-20200528/lib/gcc/riscv64-unknown-linux-gnu/8.1.0/include/stdbool.h \
  include/linux/err.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/kasan.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/linux/errno.h \
  include/linux/kernel.h \
  include/linux/math64.h \
    $(wildcard include/config/arch/supports/int128.h) \
  include/div64.h \
  include/linux/bitops.h \
  include/asm-generic/bitsperlong.h \
  arch/riscv/include/asm/bitops.h \
  arch/riscv/include/asm/system.h \
  arch/riscv/include/asm/proc-armv/system.h \
    $(wildcard include/config/cpu/sa1100.h) \
    $(wildcard include/config/cpu/sa110.h) \
    $(wildcard include/config/arm64.h) \
  include/asm-generic/bitops/fls.h \
  include/asm-generic/bitops/__fls.h \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/__ffs.h \

lib/div64.o: $(deps_lib/div64.o)

$(deps_lib/div64.o):
