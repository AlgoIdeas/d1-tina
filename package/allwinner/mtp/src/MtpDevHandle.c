#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "MtpDevHandle.h"
#include "MtpCommon.h"
#include "mtp.h"

#include <linux/usb/ch9.h>
#include <linux/usb/functionfs.h>

#define MTP_USB			"/dev/mtp_usb"

#define FFS_MTP_EP0		"/dev/usb-ffs/mtp/ep0"
#define FFS_MTP_EP_IN		"/dev/usb-ffs/mtp/ep1"
#define FFS_MTP_EP_OUT		"/dev/usb-ffs/mtp/ep2"
#define FFS_MTP_EP_INTR		"/dev/usb-ffs/mtp/ep3"

struct MtpDevOps {
	int (*open)(struct MtpDevHandle *);
	int (*read)(void *data, size_t len, struct MtpDevHandle *);
	int (*write)(void *data, size_t len, struct MtpDevHandle *);
	int (*receiveFile)(struct mtp_file_range *, bool, struct MtpDevHandle *);
	int (*sendFile)(struct mtp_file_range *, struct MtpDevHandle *);
	int (*sendEvent)(struct mtp_event *, struct MtpDevHandle *);
	int (*close)(struct MtpDevHandle *);
};

struct MtpDevHandle {
	bool is_ffs;
	struct MtpDevOps ops;
	union {
		struct {
			int mFd;
			int reserved;
		} directFd;
		struct {
			int mControl;
			int mBulkIn;
			int mBulkOut;
			int mIntr;
			void *data1;
			void *data2;
		} ffsFd;
	} descFd;
};

#define __uint16_identity(x) x
#define __uint32_identity(x) x
#define __uint64_identity(x) x

#define __bswap_constant_16(x)					\
  ((__uint16_t) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))

#define __bswap_16(x) __bswap_constant_16(x)

#define __bswap_constant_32(x)					\
  ((__uint16_t) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))

#define __bswap_32(x) __bswap_constant_32(x)

#define __bswap_constant_64(x)					\
  ((__uint16_t) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))

#define __bswap_64(x) __bswap_constant_64(x)

# if __BYTE_ORDER == __LITTLE_ENDIAN
#  define htobe16(x) __bswap_16 (x)
#  define htole16(x) __uint16_identity (x)
#  define be16toh(x) __bswap_16 (x)
#  define le16toh(x) __uint16_identity (x)

#  define htobe32(x) __bswap_32 (x)
#  define htole32(x) __uint32_identity (x)
#  define be32toh(x) __bswap_32 (x)
#  define le32toh(x) __uint32_identity (x)

#  define htobe64(x) __bswap_64 (x)
#  define htole64(x) __uint64_identity (x)
#  define be64toh(x) __bswap_64 (x)
#  define le64toh(x) __uint64_identity (x)

# else
#  define htobe16(x) __uint16_identity (x)
#  define htole16(x) __bswap_16 (x)
#  define be16toh(x) __uint16_identity (x)
#  define le16toh(x) __bswap_16 (x)

#  define htobe32(x) __uint32_identity (x)
#  define htole32(x) __bswap_32 (x)
#  define be32toh(x) __uint32_identity (x)
#  define le32toh(x) __bswap_32 (x)

#  define htobe64(x) __uint64_identity (x)
#  define htole64(x) __bswap_64 (x)
#  define be64toh(x) __uint64_identity (x)
#  define le64toh(x) __bswap_64 (x)
#endif

#define cpu_to_le16(x)  htole16(x)
#define cpu_to_le32(x)  htole32(x)

/* Direct interface(/dev/mtp_usb) */
static int MtpDevHandleDirectOpen(struct MtpDevHandle *handle)
{
	int fd;

	DLOG("");
	fd = open("/dev/mtp_usb", O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "open /dev/mtp_usb failed\n");
		return -1;
	}
	handle->descFd.directFd.mFd = fd;
	return 0;
}

static int MtpDevHandleDirectRead(void *data, size_t len, struct MtpDevHandle *handle)
{
	int fd;

	fd = handle->descFd.directFd.mFd;
	return read(fd, data, len);
}

static int MtpDevHandleDirectWrite(void *data, size_t len, struct MtpDevHandle *handle)
{
	int fd;

	fd = handle->descFd.directFd.mFd;
	return write(fd, data, len);
}

static int MtpDevHandleDirectReceiveFile(struct mtp_file_range *mfr, bool zero_packet,
					struct MtpDevHandle *handle)
{
	int ret, fd;

	fd = handle->descFd.directFd.mFd;
	ret = ioctl(fd, MTP_RECEIVE_FILE, (unsigned long)mfr);
	return ret;
}

static int MtpDevHandleDirectSendFile(struct mtp_file_range *mfr, struct MtpDevHandle *handle)
{
	int ret, fd;

	fd = handle->descFd.directFd.mFd;
	ret = ioctl(fd, MTP_SEND_FILE_WITH_HEADER, (unsigned long)mfr);
	return ret;
}

static int MtpDevHandleDirectSendEvent(struct mtp_event *em, struct MtpDevHandle *handle)
{
	int ret, fd;

	fd = handle->descFd.directFd.mFd;
	ret = ioctl(fd, MTP_SEND_EVENT, (unsigned long)em);
	return ret;
}

static int MtpDevHandleDirectClose(struct MtpDevHandle *handle)
{
	int ret;

	ret = close(handle->descFd.directFd.mFd);
	handle->descFd.directFd.mFd = -1;
	return ret;
}

/* FFS interface(/dev/usb-ffs/mtp/ep0,1,2,3) */

struct mtp_data_header {
	/* length of packet, including this header */
	__le32 length;
	/* container type (2 for data packet) */
	__le16 type;
	/* MTP command code */
	__le16 command;
	/* MTP transaction ID */
	__le32 transaction_id;
};

#define MAX_PACKET_SIZE_FS	64
#define MAX_PACKET_SIZE_HS	512
#define MAX_PACKET_SIZE_SS	1024
#define MAX_PACKET_SIZE_EV	28

struct func_desc {
    struct usb_interface_descriptor intf;
    struct usb_endpoint_descriptor_no_audio sink;
    struct usb_endpoint_descriptor_no_audio source;
    struct usb_endpoint_descriptor_no_audio intr;
} __attribute__((packed));

struct ss_func_desc {
    struct usb_interface_descriptor intf;
    struct usb_endpoint_descriptor_no_audio sink;
    struct usb_ss_ep_comp_descriptor sink_comp;
    struct usb_endpoint_descriptor_no_audio source;
    struct usb_ss_ep_comp_descriptor source_comp;
    struct usb_endpoint_descriptor_no_audio intr;
    struct usb_ss_ep_comp_descriptor intr_comp;
} __attribute__((packed));

struct desc_v1 {
    struct usb_functionfs_descs_head_v1 {
        __le32 magic;
        __le32 length;
        __le32 fs_count;
        __le32 hs_count;
    } __attribute__((packed)) header;
    struct func_desc fs_descs, hs_descs;
} __attribute__((packed));

struct desc_v2 {
    struct usb_functionfs_descs_head_v2 header;
    // The rest of the structure depends on the flags in the header.
    __le32 fs_count;
    __le32 hs_count;
    __le32 ss_count;
    __le32 os_count;
    struct func_desc fs_descs, hs_descs;
    struct ss_func_desc ss_descs;
    struct usb_os_desc_header os_header;
    struct usb_ext_compat_desc os_desc;
} __attribute__((packed));

#define STR_INTERFACE "MTP"
struct functionfs_lang {
    __le16 code;
    char str1[sizeof(STR_INTERFACE)];
} __attribute__((packed));

struct functionfs_strings {
    struct usb_functionfs_strings_head header;
    struct functionfs_lang lang0;
} __attribute__((packed));

const struct desc_v2 mtp_desc_v2 = {
	.header = {
		.magic = cpu_to_le32(FUNCTIONFS_DESCRIPTORS_MAGIC_V2),
		.length = cpu_to_le32(sizeof(struct desc_v2)),
		.flags = FUNCTIONFS_HAS_FS_DESC | FUNCTIONFS_HAS_HS_DESC |
			FUNCTIONFS_HAS_SS_DESC | FUNCTIONFS_HAS_MS_OS_DESC,
	},
	.fs_count = 4,
	.hs_count = 4,
	.os_count = 1,
	.fs_descs = {
		.intf = {
			.bLength = USB_DT_INTERFACE_SIZE,
			.bDescriptorType = USB_DT_INTERFACE,
			.bInterfaceNumber = 0,
			.bNumEndpoints = 3,
			.bInterfaceClass = USB_CLASS_STILL_IMAGE,
			.bInterfaceSubClass = 1,
			.bInterfaceProtocol = 1,
			.iInterface = 1,
		},
		.sink = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 1 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_FS,
		},
		.source = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 2 | USB_DIR_OUT,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.intr = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 3 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_INT,
			.wMaxPacketSize = MAX_PACKET_SIZE_EV,
			.bInterval = 6,
		},
	},
	.hs_descs = {
		.intf = {
			.bLength = USB_DT_INTERFACE_SIZE,
			.bDescriptorType = USB_DT_INTERFACE,
			.bInterfaceNumber = 0,
			.bNumEndpoints = 3,
			.bInterfaceClass = USB_CLASS_STILL_IMAGE,
			.bInterfaceSubClass = 1,
			.bInterfaceProtocol = 1,
			.iInterface = 1,
		},
		.sink = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 1 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.source = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 2 | USB_DIR_OUT,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.intr = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 3 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_INT,
			.wMaxPacketSize = MAX_PACKET_SIZE_EV,
			.bInterval = 6,
		},
	},
	.os_header = {
		.interface = cpu_to_le32(1),
		.dwLength = cpu_to_le32(sizeof(struct usb_os_desc_header) + sizeof(struct usb_ext_compat_desc)),
		.bcdVersion = cpu_to_le16(1),
		.wIndex = cpu_to_le16(4),
		.bCount = cpu_to_le16(1),
		.Reserved = cpu_to_le16(0),
	},
	.os_desc = {
		.bFirstInterfaceNumber = 0,
		.Reserved1 = cpu_to_le32(1),
		.CompatibleID = { 'M', 'T', 'P' },
		.SubCompatibleID = {0},
		.Reserved2 = {0},
	},
};

const struct desc_v1 mtp_desc_v1 = {
	.header = {
		.magic = cpu_to_le32(FUNCTIONFS_DESCRIPTORS_MAGIC),
		.length = cpu_to_le32(sizeof(struct desc_v1)),
		.fs_count = 4,
		.hs_count = 4,
	},
	.fs_descs = {
		.intf = {
			.bLength = USB_DT_INTERFACE_SIZE,
			.bDescriptorType = USB_DT_INTERFACE,
			.bInterfaceNumber = 0,
			.bNumEndpoints = 3,
			.bInterfaceClass = USB_CLASS_STILL_IMAGE,
			.bInterfaceSubClass = 1,
			.bInterfaceProtocol = 1,
			.iInterface = 1,
		},
		.sink = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 1 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_FS,
		},
		.source = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 2 | USB_DIR_OUT,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.intr = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 3 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_INT,
			.wMaxPacketSize = MAX_PACKET_SIZE_EV,
			.bInterval = 6,
		},
	},
	.hs_descs = {
		.intf = {
			.bLength = USB_DT_INTERFACE_SIZE,
			.bDescriptorType = USB_DT_INTERFACE,
			.bInterfaceNumber = 0,
			.bNumEndpoints = 3,
			.bInterfaceClass = USB_CLASS_STILL_IMAGE,
			.bInterfaceSubClass = 1,
			.bInterfaceProtocol = 1,
			.iInterface = 1,
		},
		.sink = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 1 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.source = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 2 | USB_DIR_OUT,
			.bmAttributes = USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize = MAX_PACKET_SIZE_HS,
		},
		.intr = {
			.bLength = USB_DT_ENDPOINT_SIZE,
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 3 | USB_DIR_IN,
			.bmAttributes = USB_ENDPOINT_XFER_INT,
			.wMaxPacketSize = MAX_PACKET_SIZE_EV,
			.bInterval = 6,
		},
	},
};

const struct functionfs_strings mtp_strings = {
    .header = {
        .magic = cpu_to_le32(FUNCTIONFS_STRINGS_MAGIC),
        .length = cpu_to_le32(sizeof(struct functionfs_strings)),
        .str_count = cpu_to_le32(1),
        .lang_count = cpu_to_le32(1),
    },
    .lang0 = {
        .code = cpu_to_le16(0x0409),
        .str1 = STR_INTERFACE,
    },
};

#if 0
static void set_udc(void)
{
	static char g_udc_controller[64];
	const char *udc_config_path = "/sys/kernel/config/usb_gadget/g1/UDC";
	int fd;

	DLOG("\n");
	if (strlen(g_udc_controller) <= 0) {
		const char *udc_path = "/sys/class/udc";
		DIR *dir;
		struct dirent *ent;
		dir = opendir(udc_path);
		if (!dir) {
			DLOG("[%s opendir %s failed]\n", __func__, udc_path);
			return;
		}
		while((ent = readdir(dir)) != NULL) {
			if (ent->d_type & DT_REG) {
				if (!strcmp(".", ent->d_name))
					continue;
				else if (!strcmp("..", ent->d_name))
					continue;
				else
					break;
			}
		}
		if (!ent) {
			DLOG("cannot found udc dir, udc driver maybe not select\n");
			exit(-1);
		}
		DLOG("%s get udc controller: %s \n", __func__, ent->d_name);
		strncpy(g_udc_controller, ent->d_name, sizeof(g_udc_controller));
		closedir(dir);
	}

	DLOG("\n");
	fd = open(udc_config_path, O_RDWR);
	if (fd<0) {
		DLOG("[%s open %s failed]\n", __func__, udc_config_path);
		return;
	}
	write(fd, g_udc_controller, strlen(g_udc_controller));

	DLOG("\n");
	close(fd);
	return;
}
#endif

int writeDescroptors(int fd)
{
	int ret;

	ret = write(fd, &mtp_desc_v2, sizeof(struct desc_v2));
	if (ret < 0) {
		fprintf(stdout, "Switching to V1 descriptor format\n");
		ret = write(fd, &mtp_desc_v1, sizeof(struct desc_v1));
		if (ret < 0) {
			fprintf(stderr, "write descriptors failed\n");
			return -1;
		}
	}

	ret = write(fd, &mtp_strings, sizeof(struct functionfs_strings));
	if (ret < 0) {
		fprintf(stderr, "write strings failed\n");
		return -1;
	}

	/*set_udc();*/
	return 0;
}

/*#define MAX_FILE_CHUNK_SIZE	(3145728)*/
#define MAX_FILE_CHUNK_SIZE	(16*1024)
#define MAX_MTP_FILE_SIZE	(0xFFFFFFFF)

static int MtpDevHandleFfsOpen(struct MtpDevHandle *handle)
{
	int fd;
	void *data = NULL;

	DLOG("");
#if 0
	fd = open(FFS_MTP_EP0, O_RDWR);
	if (fd < 0)
		goto err_open_ep;
	handle->descFd.ffsFd.mControl = fd;

	/* write descriptors */
	writeDescroptors(handle->descFd.ffsFd.mControl);
#else
	if (handle->descFd.ffsFd.mControl < 0) {
		fprintf(stderr, "ffs control fd is invalid\n");
		return -1;
	}
#endif

	fd = open(FFS_MTP_EP_IN, O_RDWR);
	if (fd < 0)
		goto err_open_ep;
	handle->descFd.ffsFd.mBulkIn = fd;
	fd = open(FFS_MTP_EP_OUT, O_RDWR);
	if (fd < 0)
		goto err_open_ep;
	handle->descFd.ffsFd.mBulkOut =  fd;
	fd = open(FFS_MTP_EP_INTR, O_RDWR);
	if (fd < 0)
		goto err_open_ep;
	handle->descFd.ffsFd.mIntr = fd;
	DLOG("");
	data = malloc(MAX_FILE_CHUNK_SIZE);
	if (!data) {
		fprintf(stderr, "%s no memory\n", __func__);
		goto err_no_memory;
	}
	handle->descFd.ffsFd.data1 = data;

	data = malloc(MAX_FILE_CHUNK_SIZE);
	if (!data) {
		fprintf(stderr, "%s no memory\n", __func__);
		goto err_no_memory;
	}
	handle->descFd.ffsFd.data2 = data;
	DLOG("");
	return 0;

err_no_memory:
	if (handle->descFd.ffsFd.data1) {
		free(handle->descFd.ffsFd.data1);
		handle->descFd.ffsFd.data1 = NULL;
	}
	if (handle->descFd.ffsFd.data2) {
		free(handle->descFd.ffsFd.data2);
		handle->descFd.ffsFd.data2 = NULL;
	}
err_open_ep:
	DLOG("");
	fprintf(stderr, "open ep failed\n");
	if (handle->descFd.ffsFd.mControl > 0)
		close(handle->descFd.ffsFd.mControl);
	if (handle->descFd.ffsFd.mBulkIn > 0)
		close(handle->descFd.ffsFd.mBulkIn);
	if (handle->descFd.ffsFd.mBulkOut > 0)
		close(handle->descFd.ffsFd.mBulkOut);
	if (handle->descFd.ffsFd.mIntr > 0)
		close(handle->descFd.ffsFd.mIntr);
	handle->descFd.ffsFd.mControl = -1;
	handle->descFd.ffsFd.mBulkIn = -1;
	handle->descFd.ffsFd.mBulkOut = -1;
	handle->descFd.ffsFd.mIntr = -1;
	return -1;
}

static int MtpDevHandleFfsRead(void *data, size_t len, struct MtpDevHandle *handle)
{
	int fd;

	fd = handle->descFd.ffsFd.mBulkOut;
	return read(fd, data, len);
}

static int MtpDevHandleFfsWrite(void *data, size_t len, struct MtpDevHandle *handle)
{
	int fd;

	fd = handle->descFd.ffsFd.mBulkIn;
	return write(fd, data, len);

}

static inline uint32_t min(uint32_t a, uint32_t b)
{
	if (a < b)
		return a;
	return b;
}

static int MtpDevHandleFfsReceiveFile(struct mtp_file_range *mfr, bool zero_packet,
					struct MtpDevHandle *handle)
{
	int ret = -1;
	int fd = mfr->fd;
	uint32_t file_length = mfr->length;
	uint64_t offset = mfr->offset;
	size_t length;
	void *data1 = handle->descFd.ffsFd.data1;
	void *data2 = handle->descFd.ffsFd.data2;
	void *data = data1;
	struct {
		void *data;
		uint32_t len;
	} wData;

	bool iread = false;
	bool iwrite = false;

	DLOG("");
	while (file_length > 0 || iwrite) {
		if (file_length > 0) {
			length = min(MAX_FILE_CHUNK_SIZE, file_length);
			ret = MtpDevHandleFfsRead(data, length, handle);
			if (ret != MAX_MTP_FILE_SIZE && ret < length) {
				ret = -1;
				errno = EIO;
			}
			iread = true;
		}
		if (iwrite) {
			ssize_t written;
			written = pwrite(fd, wData.data, wData.len, offset);
			if (written < 0) {
				ret = -1;
				errno = EIO;
			}
			iwrite = false;
		}
		if (ret == -1) {
			return -1;
		}
		if (iread) {
			if (file_length == MAX_MTP_FILE_SIZE) {
				if (ret < length) {
					file_length = 0;
				}
			} else {
				file_length -= ret;
			}
			offset += ret;
			wData.data = data;
			wData.len = ret;
			if (data == data1) {
				data = data2;
			} else {
				data = data1;
			}
			iwrite = true;
			iread = false;
		}
	}
#if 0
	if (ret % packet_size == 0 || zero_packet) {
		MtpDevHandleFfsRead(data, packet_size, handle);
	}
#endif
	return 0;
}

static int MtpDevHandleFfsSendFile(struct mtp_file_range *mfr, struct MtpDevHandle *handle)
{
	int ret = -1;
	int fd = mfr->fd;
	uint32_t file_length = mfr->length;
	uint64_t offset = mfr->offset;
	size_t length;
	void *data1 = handle->descFd.ffsFd.data1;
	void *data2 = handle->descFd.ffsFd.data2;
	void *data = data1;
	struct {
		void *data;
		uint32_t len;
	} wData;

	bool iread = false;
	bool iwrite = false;
	int error = 0;

	int packet_size = 512;
	int init_read_len = min(packet_size - sizeof(struct mtp_data_header), file_length);
	struct mtp_data_header *header = NULL;
	uint32_t given_length = file_length + sizeof(struct mtp_data_header);

	if (given_length > MAX_MTP_FILE_SIZE)
		given_length = MAX_MTP_FILE_SIZE;

	DLOG("given_length=%u\n", given_length);
	header = (struct mtp_data_header *)data;
	header->length = cpu_to_le32(given_length);
	header->type = cpu_to_le16(2); /*  data packet */
	header->command = cpu_to_le16(mfr->command);
	header->transaction_id = cpu_to_le32(mfr->transaction_id);

	pread(fd, data + sizeof(struct mtp_data_header), init_read_len, offset);
	MtpDevHandleFfsWrite(data, sizeof(struct mtp_data_header) + init_read_len, handle);

	file_length -= init_read_len;
	offset += init_read_len;
	ret = init_read_len + sizeof(struct mtp_data_header);

	while (file_length > 0) {
		if (iread) {
			file_length -= ret;
			offset += ret;
			wData.data = data;
			wData.len = ret;
			if (data == data1) {
				data = data2;
			} else {
				data = data1;
			}
			iwrite = true;
			iread = false;
		}
		if (error == -1)
			return -1;
		if (file_length > 0) {
			length = min(MAX_FILE_CHUNK_SIZE, file_length);
			ret = read(fd, data, length);
			iread = true;
		}
		if (iwrite) {
			if (MtpDevHandleWrite(wData.data, wData.len, handle) < 0)
				error = -1;
			iwrite = false;
		}
	}
#if 0
	if (ret % packet_size == 0) {
		MtpDevHandleFfsWrite(data, packet_size, handle);
	}
#endif
	return 0;
}

static int MtpDevHandleFfsSendEvent(struct mtp_event *em, struct MtpDevHandle *handle)
{
	int fd;

	fd = handle->descFd.ffsFd.mIntr;
	return write(fd, em->data, em->length);
}

static int MtpDevHandleFfsClose(struct MtpDevHandle *handle)
{
#if 0
	if (handle->descFd.ffsFd.mControl > 0)
		close(handle->descFd.ffsFd.mControl);
#endif
	if (handle->descFd.ffsFd.mBulkIn > 0)
		close(handle->descFd.ffsFd.mBulkIn);
	if (handle->descFd.ffsFd.mBulkOut > 0)
		close(handle->descFd.ffsFd.mBulkOut);
	if (handle->descFd.ffsFd.mIntr > 0)
		close(handle->descFd.ffsFd.mIntr);
	/*handle->descFd.ffsFd.mControl = -1;*/
	handle->descFd.ffsFd.mBulkIn = -1;
	handle->descFd.ffsFd.mBulkOut = -1;
	handle->descFd.ffsFd.mIntr = -1;

	if (handle->descFd.ffsFd.data1) {
		free(handle->descFd.ffsFd.data1);
		handle->descFd.ffsFd.data1 = NULL;
	}
	if (handle->descFd.ffsFd.data2) {
		free(handle->descFd.ffsFd.data2);
		handle->descFd.ffsFd.data2 = NULL;
	}
	return 0;
}

/* MtpDevHandle Interface */
struct MtpDevHandle *MtpDevHandleInit(int fd)
{
	int ret = 0;
	struct MtpDevHandle *handle;
	bool ffs_ok;


	handle = (struct MtpDevHandle *)calloc_wrapper(1, sizeof(struct MtpDevHandle));
	if (!handle) {
		fprintf(stderr, "no memory\n");
		return NULL;
	}

	ffs_ok = access(FFS_MTP_EP0, W_OK) == 0;
	DLOG("ffs_ok is %s\n", ffs_ok ? "true" : "false");
	if (ffs_ok) {
		handle->ops.open = MtpDevHandleFfsOpen;
		handle->ops.write = MtpDevHandleFfsWrite;
		handle->ops.read = MtpDevHandleFfsRead;
		handle->ops.receiveFile = MtpDevHandleFfsReceiveFile;
		handle->ops.sendFile = MtpDevHandleFfsSendFile;
		handle->ops.sendEvent = MtpDevHandleFfsSendEvent;
		handle->ops.close = MtpDevHandleFfsClose;
	} else {
		handle->ops.open = MtpDevHandleDirectOpen;
		handle->ops.write = MtpDevHandleDirectWrite;
		handle->ops.read = MtpDevHandleDirectRead;
		handle->ops.receiveFile = MtpDevHandleDirectReceiveFile;
		handle->ops.sendFile = MtpDevHandleDirectSendFile;
		handle->ops.sendEvent = MtpDevHandleDirectSendEvent;
		handle->ops.close = MtpDevHandleDirectClose;
	}
	handle->is_ffs = ffs_ok;

	if (ffs_ok) {
		handle->descFd.ffsFd.mControl = fd;
	}
	ret = handle->ops.open(handle);
	if (ret != 0) {
		fprintf(stderr, "%s ops open failed\n", ffs_ok ?
						"ffs" : "direct");
		free_wrapper(handle);
		return NULL;
	}

	return handle;
}

int MtpDevHandleRelease(struct MtpDevHandle *handle)
{
	int ret;

	ret = handle->ops.close(handle);
	free_wrapper(handle);
	return 0;
}

int MtpDevHandleRead(void *data, size_t len, struct MtpDevHandle *handle)
{
	if (!handle || !handle->ops.read)
		return -1;
	return handle->ops.read(data, len, handle);
}

int MtpDevHandleWrite(void *data, size_t len, struct MtpDevHandle *handle)
{
	if (!handle || !handle->ops.write)
		return -1;
	return handle->ops.write(data, len, handle);
}

int MtpDevHandleSendFile(struct mtp_file_range *mfr, struct MtpDevHandle *handle)
{
	if (!handle || !handle->ops.sendFile)
		return -1;
	return handle->ops.sendFile(mfr, handle);
}

int MtpDevHandleReceiveFile(struct mtp_file_range *mfr, bool zero_packet, struct MtpDevHandle *handle)
{
	if (!handle || !handle->ops.receiveFile)
		return -1;
	return handle->ops.receiveFile(mfr, zero_packet, handle);
}

int MtpDevHandleSendEvent(struct mtp_event *em, struct MtpDevHandle *handle)
{
	if (!handle || !handle->ops.sendEvent)
		return -1;
	return handle->ops.sendEvent(em, handle);
}
