/*
 * Copyright (C) 2016 The AllWinner Project
 */

#define LOG_TAG "nativepower_daemon"
#include <tina_log.h>

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#include "np_autosuspend.h"
#include "power_manager.h"
#include "user_activity.h"
#include "np_input.h"

#include "np_wakelock.h"
#include "np_scene_manager.h"


#ifdef USE_DBUS
#include "dbus_server.h"
static pthread_t dbus_thread;
#endif

#define   NATIVE_POWER_WAKELOCK    "native_power_lock"

void wakeup_callback(bool sucess)
{
	acquire_wake_lock(NATIVE_POWER_WAKELOCK);
	invalidateActivityTime();
}

int main(int argc, char **argv)
{
	int ret;
	char buf[80];

	/* we shuld to require wakelock to prevent sutosleep */
	acquire_wake_lock(NATIVE_POWER_WAKELOCK);

	/* enable autosleep*/
	autosuspend_enable();
	set_wakeup_callback(wakeup_callback);

	/* init awake timout from config file */
	init_awake_timeout();

	/* start input event scan */
	np_input_init();

	/* enable log */
	openlog("NativePower", LOG_NOWAIT, LOG_DAEMON);

#ifdef USE_DBUS
	ret = pthread_create(&dbus_thread, NULL, dbus_server_thread_func, NULL);
	if (ret) {
		strerror_r(ret, buf, sizeof(buf));
		goto end;
	}
#endif

	while (1) {
		if (!isActivityTimeValid()) {
			release_wake_lock(NATIVE_POWER_WAKELOCK);
		}

		sleep(1);
	}

end:
	autosuspend_disable();
	TLOGE(LOG_TAG, "EXIT ERROR!!!!");

	return -1;
}
