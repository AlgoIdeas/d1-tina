/*
 * Copyright (C) 2016 The AllWinner Project
 */

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "user_activity.h"
#include "power_manager.h"
#include "np_autosuspend.h"
#include "np_scene_utils.h"

#define AWAKE_TIMEOUT_LIMIT	10

static pthread_mutex_t mLock = PTHREAD_MUTEX_INITIALIZER;
static time_t mLastUserActivityTime = -1;
static time_t mKeepAwakeTime = -1;

int userActivity(void)
{
	pthread_mutex_lock(&mLock);
	mLastUserActivityTime = time(NULL);
	pthread_mutex_unlock(&mLock);

	return 0;
}

int isActivityTimeValid(void)
{
	time_t timenow;
	time_t awakeTimeout = -1;
	char cAwakeTimeout[32];

	memset(cAwakeTimeout, 0, sizeof(cAwakeTimeout));
	np_get_scene_property("awake_timeout", cAwakeTimeout, sizeof(cAwakeTimeout));
	awakeTimeout = atoll(cAwakeTimeout);
	if (mKeepAwakeTime != awakeTimeout && awakeTimeout > AWAKE_TIMEOUT_LIMIT) {
		pthread_mutex_lock(&mLock);
		mKeepAwakeTime = awakeTimeout;
		pthread_mutex_unlock(&mLock);
	}

	if (mKeepAwakeTime < 0) {
		return 1;
	}

	timenow = time(NULL);
	if (timenow < mLastUserActivityTime || mLastUserActivityTime <= 0) {
		mLastUserActivityTime = timenow;
	}

	if ((timenow - mLastUserActivityTime) < mKeepAwakeTime) {
		return 1;
	} else {
		mLastUserActivityTime = 0;
		return 0;
	}
}

int invalidateActivityTime(void)
{
	mLastUserActivityTime = -1;

	return 0;
}

int init_awake_timeout(void)
{
	char cAwakeTimeout[32];
	time_t awakeTimeout = -1;
	int ret = -1;

	memset(cAwakeTimeout, 0, sizeof(cAwakeTimeout));
	ret = np_get_scene_property("awake_timeout", cAwakeTimeout, sizeof(cAwakeTimeout));

	awakeTimeout = atoll(cAwakeTimeout);

	if (awakeTimeout > 0) {
		pthread_mutex_lock(&mLock);
		mKeepAwakeTime = awakeTimeout;
		pthread_mutex_unlock(&mLock);
		ret = 0;
	}

	return ret;
}

int set_awake_timeout(long timeout_s)
{
	char cAwakeTimeout[32];
	int ret = -1;

	if (timeout_s < AWAKE_TIMEOUT_LIMIT)
		return -1;

	pthread_mutex_lock(&mLock);
	mKeepAwakeTime = (time_t) timeout_s;
	pthread_mutex_unlock(&mLock);

	sprintf(cAwakeTimeout, "%ld", timeout_s);
	ret = np_set_scene_property("awake_timeout", cAwakeTimeout);

	return ret;
}

