
/*
 * Copyright (C) 2016 The AllWinner Project
 */

#ifndef _NATIVEPOWER_DAEMON_NP_INPUT_H_
#define _NATIVEPOWER_DAEMON_NP_INPUT_H_

#if __cplusplus
extern "C" {
#endif

int np_input_init();

#if __cplusplus
} // extern "C"
#endif

#endif
