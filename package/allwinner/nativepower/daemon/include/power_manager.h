
/*
 * Copyright (C) 2016 The AllWinner Project
 */

#ifndef _NATIVEPOWER_DAEMON_POWER_MANAGER_H_
#define _NATIVEPOWER_DAEMON_POWER_MANAGER_H_

#if __cplusplus
extern "C" {
#endif

int suspend(void);
int shutdown(void);
int reboot(void);

#if __cplusplus
} // extern "C"
#endif

#endif
