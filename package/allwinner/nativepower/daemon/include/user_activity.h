
/*
 * Copyright (C) 2016 The AllWinner Project
 */

#ifndef _NATIVEPOWER_DAEMON_USER_ACTIVITY_H_
#define _NATIVEPOWER_DAEMON_USER_ACTIVITY_H_

#if __cplusplus
extern "C" {
#endif

int userActivity(void);
int isActivityTimeValid(void);
int invalidateActivityTime(void);

int init_awake_timeout(void);
int set_awake_timeout(long timeout_s);

#if __cplusplus
} // extern "C"
#endif

#endif
