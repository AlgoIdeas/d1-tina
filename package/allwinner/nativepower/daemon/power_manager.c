/*
 * Copyright (C) 2016 The AllWinner Project
 */

#define LOG_TAG "nativepower_daemon"
#include <tina_log.h>

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "np_wakelock.h"
#include "power_manager.h"


int suspend(void)
{
	return release_wake_lock_all();
}


int shutdown(void)
{
	int pid = fork();
	int ret = -1;

	if (pid == 0) {
		execlp("/sbin/poweroff", "poweroff", NULL);
	}

	return 0;
}

int reboot(void)
{
	int pid = fork();
	int ret = -1;

	if (pid == 0) {
		execlp("/sbin/reboot", "reboot", NULL);
	}

	return 0;
}
