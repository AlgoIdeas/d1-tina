/*
 * Copyright (C) 2016 The AllWinner Project
 */

#define LOG_TAG "nativepower_daemon"
#include <tina_log.h>

#include <pthread.h>
#include <linux/input.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <glob.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "user_activity.h"
#include "power_manager.h"

#define DEV_INPUT_KEY_PATH "/dev/input/event*"

#define KEY_PRESS_UP    0x0
#define KEY_PRESS_DOWN  0x1

#define MSEC_PER_SEC            (1000LL)
#define NSEC_PER_MSEC           (1000000LL)


static pthread_t thread_key_power;

static int open_input(int *fd_array, int *num)
{
	char *filename = NULL;
	glob_t globbuf;
	unsigned i;
	int success = 0;
	int fd_num = 0;

	if (!fd_array || !num)
		return -1;
	/* get all the matching event filenames */
	glob(DEV_INPUT_KEY_PATH, 0, NULL, &globbuf);

	/* for each event file */
	if (globbuf.gl_pathc > *num)
		return -1;
	for (i = 0; i < globbuf.gl_pathc; ++i) {
		filename = globbuf.gl_pathv[i];

		/* open this input layer device file */
		fd_array[fd_num] = open(filename, O_RDONLY | O_NONBLOCK | O_CLOEXEC);
		if (fd_array[fd_num] >= 0) {
			fd_num++;
			success = 1;
		}
	}
	*num = fd_num;
	globfree(&globbuf);
	if (!success)
		return -1;

	return 0;
}

static uint64_t curr_time_ms(void)
{
	struct timespec tm;
	clock_gettime(CLOCK_MONOTONIC, &tm);
	return tm.tv_sec * MSEC_PER_SEC + (tm.tv_nsec / NSEC_PER_MSEC);
}


static int  key_event_update(struct input_event *key_event)
{
	int  ret = 0;
	uint64_t now = 0;
	static uint64_t powerkey_down_ms;

	TLOGV("\tcall %s %d\n", __func__, __LINE__);
	switch (key_event->type) {
	case EV_KEY:
		switch (key_event->code) {
		case KEY_POWER:
			TLOGV("---------KEY_POWER,value=%d---------\n", key_event->value);

			switch (key_event->value) {
			case KEY_PRESS_DOWN:
				powerkey_down_ms = curr_time_ms();
				TLOGV("\tDOWN: %u\n", powerkey_down_ms);
			break;

			case KEY_PRESS_UP:
				now = curr_time_ms();
				TLOGV("\tUP: %u\n", now);
				TLOGV("\tDIFF: %u\n", now-powerkey_down_ms);

				if (now <= powerkey_down_ms) {
					TLOGV("\tcall %s %d\n", __func__, __LINE__);
					/* invalid powerkey_down_ms */
					powerkey_down_ms = 0;
				} else if (now - powerkey_down_ms > 6000UL) {
					TLOGV("\tcall %s %d\n", __func__, __LINE__);
					/* powerkey_down_ms more than POWERDOWN_MIN_LIMIT */
					/* we request to shutdown */
					shutdown();
				} else if (now - powerkey_down_ms < 2000) {
					TLOGV("\tcall %s %d\n", __func__, __LINE__);
					/* powerkey_down_ms less than SUSPEND_MAX_LIMIT */
					/* we release wakelock, let system can to sleep */
					ret = suspend();
					if (ret)
						TLOGE("suspend failed, return %d\n", ret);
				} else {
					/* click are treates as user activities */
					TLOGV("\tcall %s %d\n", __func__, __LINE__);
					userActivity();
				}
			break;

			default:
			break;
			} /* end of swtich(key_event->value) */
		break;

		default:
			/* All keystroke events are treated as user activities */
			userActivity();
		break;
		} /* end of swtich(key_event->code) */
	break;

	default:
	break;
	} /* end of swtich(key_event->type) */

	return 0;
}


/*=============================================================
 * Function: scankey
 * Descriptions:
 *   the thread to scan key
 *============================================================*/
static void *scankey(void *arg)
{
	struct epoll_event ev;
	int epollfd, i, input_event_count = 128;
	int input_event_fd[input_event_count];
	struct epoll_event events[input_event_count];
	struct input_event key_event;
	int nevents, timeout = -1;

	epollfd = epoll_create(40);
	if (epollfd == -1) {
		TLOGE("epoll create failed.\n");
		return NULL;
	}

	open_input(input_event_fd, &input_event_count);
	for (i = 0; i < input_event_count; i++) {
		ev.data.fd = input_event_fd[i];
		ev.events = EPOLLIN | EPOLLWAKEUP;
		if (epoll_ctl(epollfd, EPOLL_CTL_ADD, input_event_fd[i], &ev)) {
			TLOGE("epoll_ctl failed.\n");
			return NULL;
		}
	}

	while (1) {
		memset(&events, 0, sizeof(events));
		nevents = epoll_wait(epollfd, events, input_event_count, timeout);

		if (nevents == -1) {
			if (errno == EINTR)
				continue;
			TLOGE("epoll_wait failed.\n");
			break;
		}

		for (i = 0; i < nevents; i++) {
			if (read(events[i].data.fd, &key_event, sizeof(struct input_event))\
					== sizeof(struct input_event)) {
				key_event_update(&key_event);
			}
		}
	}
}

int np_input_init(void)
{
	int res = 0;

	res = pthread_create(&thread_key_power, NULL, scankey, NULL);
	if (res < 0) {
		TLOGE("\tcreate thread of scankey failed\n");
		return -1;
	}

	res = pthread_detach(thread_key_power);
	if (res < 0) {
		TLOGE("\tset scankey detached failed\n");
		return -1;
	}

	return res;
}
