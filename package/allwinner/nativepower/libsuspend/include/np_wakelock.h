/*
 * Copyright (C) 2016 The AllWinner Project
 */

#ifndef _LIBSUSPEND_NP_WAKELOCK_H_
#define _LIBSUSPEND_NP_WAKELOCK_H_

#include <stdint.h>

#if __cplusplus
extern "C" {
#endif

int acquire_wake_lock(const char *id);
int release_wake_lock(const char *id);
int release_wake_lock_all(void);
int get_wake_lock_count(void);

#if __cplusplus
} // extern "C"
#endif

#endif
