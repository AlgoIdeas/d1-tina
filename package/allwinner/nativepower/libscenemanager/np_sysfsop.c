/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <tina_log.h>

#include "np_sysfsop.h"
#include "np_uci_config.h"
#include "np_scene_utils.h"
#include "SceneConfig.h"

static int getConfig(const char *path, char *value, size_t size)
{
	int fd = -1;

	if (!strcmp(path, "/dev/null")) {
		TLOGI("try open null device!\n");
		return -1;
	}

	fd = open(path, O_RDONLY);
	TLOGI("open %s return %d\n", path, fd);

	if (fd < 0)
		return -1;

	read(fd, value, size);

	close(fd);
	return 0;
}

static int setConfig(const char *path, const char *value)
{
	int fd = -1;

	TLOGI("path:%s set value:%s", path, value);

	if (!strcmp(path, "/dev/null")) {
		TLOGI("try open null device!\n");
		return -1;
	}

	fd = open(path, O_WRONLY);
	if (fd < 0)
		return -1;

	write(fd, value, strlen(value));

	close(fd);
	return 0;
}

static const char *GetActualFreq(char *freq, const char *avail_freq_path)
{
	int ret = 0;

	if (!strcmp(freq, "max"))
		ret = 1;
	else if (!strcmp(freq, "min"))
		ret = 2;

	if (ret != 0) {
		char value[256];
		char *token = NULL;
		size_t len;

		memset(value, 0, sizeof(value));
		if (getConfig(avail_freq_path, value, sizeof(value)) != 0) {
			fprintf(stderr, "avail_freq_path:%s unknown\n", avail_freq_path);
			return NULL;
		}
		TLOGI("avail_freq: %s", value);
		len = strlen(value);
		token = strtok(value, " ");
		if (token == NULL)
			return NULL;
		if (ret == 2)
			strcpy(freq, token);
		else {
			char *token_pre;
			do {
				TLOGI("freq: %s", token);
				token_pre = token;
				token = strtok(NULL, " ");
			} while (token != NULL && (len - (token - value) > 2));
			strcpy(freq, token_pre);
		}
	}
	return freq;
}

static int SetBootLock(const char *bootlock)
{
	return setConfig(CPU0LOCK, bootlock);
}

static int SetRoomage(const char *roomage)
{
	return setConfig(ROOMAGE, roomage);
}

static int SetCpuFreq(const char *cpu_freq)
{
	char actualfreq[32];

	strcpy(actualfreq, cpu_freq);

	if (!GetActualFreq(actualfreq, CPUFREQ_AVAIL))
		return -1;

	setConfig(CPUFREQ_MAX, actualfreq);
	setConfig(CPUFREQ_MIN, actualfreq);

	return 0;
}

static int SetCpuFreqMax(const char *cpu_freq_max)
{
	return setConfig(CPUFREQ_MAX, cpu_freq_max);
}

static int SetCpuFreqMin(const char *cpu_freq_min)
{
	return setConfig(CPUFREQ_MIN, cpu_freq_min);
}

static int GetCpuFreq(char *buf, size_t len)
{
	return getConfig(CPUFREQ, buf, len);
}

static int GetCpuOnline(char *buf, size_t len)
{
	return getConfig(CPUONLINE, buf, len);
}

static int GetCpuGov(char *buf, size_t len)
{
	return getConfig(CPU0GOV, buf, len);
}

static int SetCpuGov(const char *cpu_gov)
{
	return setConfig(CPU0GOV, cpu_gov);
}

static int SetCpuHot(const char *cpu_hot)
{
	return setConfig(CPUHOT, cpu_hot);
}

static int SetCpuOnline(const char *cpu_online)
{
	char online[CPU_NUM_MAX];
	int num;

	if (!strcmp(cpu_online, "all")) {
		memset(online, 1, sizeof(online));
	} else {
		char cpu_online_tmp[sizeof(((CPU_SCENE *) 0)->cpu_online)];
		char *token = NULL;
		memset(online, 0, sizeof(online));
		memcpy(cpu_online_tmp, cpu_online, sizeof(cpu_online_tmp));
		token = strtok(cpu_online_tmp, "-,");
		while (token != NULL) {
			num = atoi(token);
			if (num < 0 || num > (CPU_NUM_MAX - 1))
				return -1;
			online[num] = 1;
			if (cpu_online[token - cpu_online_tmp + 1] == '-') {
				int start = num;
				int end, i;
				if (token - cpu_online_tmp + 2 < sizeof(cpu_online_tmp))
					end = cpu_online[token - cpu_online_tmp + 2] - '0';
				else
					return -1;
				if (end < start)
					return -1;
				for (i = start; i <= end; i++)
					online[i] = 1;
			}
			token = strtok(NULL, "-,");
		}
	}
	/* ignore CPU0 */
	for (num = 1; num < CPU_NUM_MAX; num++) {
		char buf[128];
		snprintf(buf, sizeof(buf), CPUxONLINE, num);
		setConfig(buf, online[num] != 0 ? "1" : "0");
	}
	return 0;
}

static int SetDramFreqAdaptive(const char *pause)
{
	return setConfig(DRAMMODE, pause);
}

static int SetDramFreq(const char *dram_freq)
{
	char actualfreq[32];

	strcpy(actualfreq, dram_freq);

	if (!GetActualFreq(actualfreq, DRAMFREQ_AVAIL))
		return -1;

	if (atoi(actualfreq)/1000000U > 0)
		snprintf(actualfreq, sizeof(actualfreq), "%d", atoi(actualfreq) / 1000);

	setConfig(DRAMFREQ_MAX, actualfreq);
	setConfig(DRAMFREQ_MIN, actualfreq);

	return 0;
}

static int SetDramFreqMax(const char *dram_freq_max)
{
	return setConfig(DRAMFREQ_MAX, dram_freq_max);
}

static int SetDramFreqMin(const char *dram_freq_min)
{
	return setConfig(DRAMFREQ_MIN, dram_freq_min);
}

static int GetDramFreq(char *buf, size_t len)
{
	return getConfig(DRAMFREQ, buf, len);
}

static int SetGpuFreq(const char *gpu_freq)
{
	return -1;
}

static int GetGpuFreq(char *gpu_freq, int len)
{
	return -1;
}

static CPU_SCENE_OPS cpu_ops = {
	.SetBootLock = SetBootLock,
	.SetRoomAge = SetRoomage,
	.SetCpuFreq = SetCpuFreq,
	.SetCpuFreqMax = SetCpuFreqMax,
	.SetCpuFreqMin = SetCpuFreqMin,
	.SetCpuGov = SetCpuGov,
	.SetCpuHot = SetCpuHot,
	.SetCpuOnline = SetCpuOnline,

	.GetCpuFreq = GetCpuFreq,
	.GetCpuGov = GetCpuGov,
	.GetCpuOnline = GetCpuOnline,
};

static GPU_SCENE_OPS gpu_ops = {
	.SetGpuFreq = SetGpuFreq,
	.GetGpuFreq = GetGpuFreq,
};

static DRAM_SCENE_OPS dram_ops = {
	.SetDramFreqAdaptive = SetDramFreqAdaptive,
	.SetDramFreq = SetDramFreq,
	.SetDramFreqMax = SetDramFreqMax,
	.SetDramFreqMin = SetDramFreqMin,

	.GetDramFreq = GetDramFreq,
};

void *np_get_sysfsops(int index)
{
	void *ops = NULL;

	switch (index) {
	case CPU_INDEX:
		ops = &cpu_ops;
		break;
	case GPU_INDEX:
		ops = &dram_ops;
		break;
	case DRAM_INDEX:
		ops = &gpu_ops;
		break;
	}
	return ops;
}
