/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _NP_SCENE_MANAGER_H_
#define _NP_SCENE_MANAGER_H_

#include <unistd.h>
#include "np_scene.h"

#if __cplusplus
extern "C" {
#endif

int np_scene_set(NP_SCENE *scene);
int np_scene_change(const char *scene_name);

int np_cpu_freq_get(char *freq, size_t len);
int np_cpu_freq_set(const char *freq);
int np_cpu_gov_get(char *gov, size_t len);
int np_cpu_online_get(char *online, size_t len);

int np_dram_freq_get(char *freq, size_t len);
int np_dram_freq_set(const char *freq);

int np_gpu_freq_get(char *freq, size_t len);
int np_gpu_freq_set(const char *freq);

#if __cplusplus
}
#endif

#endif
