/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _NP_SCENE_UTILS_H_
#define _NP_SCENE_UTILS_H_

#include "np_scene.h"

#if __cplusplus
extern "C" {
#endif

int np_get_property(const char *conf_section, const char *conf_name, char *scene, size_t len);
int np_set_property(const char *conf_section, const char *conf_name, const char *scene);
int np_get_scene_config(NP_SCENE *scene, const char *scene_name);

int np_get_cur_scene_name(char *scene_name, size_t len);
int np_set_cur_scene_name(const char *scene_name);
int np_get_scene_property(const char *conf_name, char *conf_buf, size_t len);
int np_set_scene_property(char *conf_name, char *conf_buf);

#if __cplusplus
}
#endif

#endif

