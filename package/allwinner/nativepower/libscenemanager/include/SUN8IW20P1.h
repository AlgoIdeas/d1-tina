#ifndef _NP_SUN8IW20P1_H_
#define _NP_SUN8IW20P1_H_

/* cpu spec files defined */
#define CPU_NUM_MAX     4
#define CPU0LOCK        "/dev/null"
#define ROOMAGE         "/dev/null"
#define CPUFREQ_AVAIL   "/sys/devices/system/cpu/cpufreq/policy0/scaling_available_frequencies"
#define CPUFREQ         "/sys/devices/system/cpu/cpufreq/policy0/cpuinfo_cur_freq"
#define CPUFREQ_MAX     "/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq"
#define CPUFREQ_MIN     "/sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq"
#define CPU0GOV         "/sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
#define CPUONLINE       "/sys/devices/system/cpu/online"
#define CPUxONLINE       "/sys/devices/system/cpu/cpu%d/online"
#define CPUHOT          "/dev/null"

/* gpu spec files defined */
#define GPUFREQ         "/dev/null"
#define GPUCOMMAND      "/dev/null"

/* ddr spec files defined */
#define DRAMFREQ_AVAIL  "/dev/null"
#define DRAMFREQ        "/dev/null"
#define DRAMFREQ_MAX    "/dev/null"
#define DRAMFREQ_MIN    "/dev/null"
#define DRAMMODE        "/dev/null"

/* task spec files defined */
#define TASKS           "/dev/null"

/* touch screen runtime suspend */
#define TP_SUSPEND      "/dev/null"

#endif
