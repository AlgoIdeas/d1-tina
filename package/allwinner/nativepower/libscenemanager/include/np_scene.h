/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _NP_SCENE_H_
#define _NP_SCENE_H_

#if __cplusplus
extern "C" {
#endif

typedef struct {
	char bootlock[4];
	char cpu_freq[16];
	char cpu_freq_max[16];
	char cpu_freq_min[16];
	char roomage[64];
	char cpu_gov[16];
	char cpu_hot[16];
	char cpu_online[16];
} CPU_SCENE;

typedef struct {
	char gpu_freq[16];
} GPU_SCENE;

typedef struct {
	char dram_adaptive[4];
	char dram_freq[16];
	char dram_freq_max[16];
	char dram_freq_min[16];
} DRAM_SCENE;

typedef struct {
	CPU_SCENE cpu;
	GPU_SCENE gpu;
	DRAM_SCENE dram;
} NP_SCENE;

#if __cplusplus
}
#endif

#endif



