/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <tina_log.h>

#include "np_uci_config.h"
#include "np_scene_utils.h"

int np_get_property(const char *conf_section, const char *conf_name, char *property, size_t len)
{
	NP_UCI *uci = np_uci_open(NATIVE_POWER_CONFIG_PATH);
	if (uci == NULL)
		return -1;
	np_uci_read_config(uci, conf_section, conf_name, property, len);
	np_uci_close(uci);
	return 0;
}

int np_set_property(const char *conf_section, const char *conf_name, const char *property)
{
	NP_UCI *uci = np_uci_open(NATIVE_POWER_CONFIG_PATH);
	if (uci == NULL)
		return -1;
	np_uci_write_config(uci, conf_section, conf_name, property);
	np_uci_close(uci);
	return 0;
}

int np_get_scene_config(NP_SCENE * scene, const char *scene_name)
{
	int ret = 0;
	NP_UCI *uci = np_uci_open(NATIVE_POWER_CONFIG_PATH);
	if (uci == NULL)
		return -1;

	ret += np_uci_read_config(uci, scene_name, "bootlock", scene->cpu.bootlock, sizeof(scene->cpu.bootlock));
	ret += np_uci_read_config(uci, scene_name, "roomage", scene->cpu.roomage, sizeof(scene->cpu.roomage));
	ret += np_uci_read_config(uci, scene_name, "cpu_freq", scene->cpu.cpu_freq, sizeof(scene->cpu.cpu_freq));
	ret += np_uci_read_config(uci, scene_name, "cpu_freq_max", scene->cpu.cpu_freq_max, sizeof(scene->cpu.cpu_freq_max));
	ret += np_uci_read_config(uci, scene_name, "cpu_freq_min", scene->cpu.cpu_freq_min, sizeof(scene->cpu.cpu_freq_min));
	ret += np_uci_read_config(uci, scene_name, "cpu_gov", scene->cpu.cpu_gov, sizeof(scene->cpu.cpu_gov));
	ret += np_uci_read_config(uci, scene_name, "cpu_hot", scene->cpu.cpu_hot, sizeof(scene->cpu.cpu_hot));
	ret += np_uci_read_config(uci, scene_name, "cpu_online", scene->cpu.cpu_online, sizeof(scene->cpu.cpu_online));

	ret += np_uci_read_config(uci, scene_name, "gpu_freq", scene->gpu.gpu_freq, sizeof(scene->gpu.gpu_freq));

	ret += np_uci_read_config(uci, scene_name, "dram_adaptive", scene->dram.dram_adaptive, sizeof(scene->dram.dram_adaptive));
	ret += np_uci_read_config(uci, scene_name, "dram_freq", scene->dram.dram_freq, sizeof(scene->dram.dram_freq));
	ret += np_uci_read_config(uci, scene_name, "dram_freq_max", scene->dram.dram_freq_max, sizeof(scene->dram.dram_freq_max));
	ret += np_uci_read_config(uci, scene_name, "dram_freq_min", scene->dram.dram_freq_min, sizeof(scene->dram.dram_freq_min));

	TLOGI("get uci config:%s\n", scene_name);
	TLOGI("bootlock:%s", scene->cpu.bootlock);
	TLOGI("roomage:%s", scene->cpu.roomage);
	TLOGI("cpu_freq:%s", scene->cpu.cpu_freq);
	TLOGI("cpu_freq_max:%s", scene->cpu.cpu_freq_max);
	TLOGI("cpu_freq_min:%s", scene->cpu.cpu_freq_min);
	TLOGI("cpu_gov:%s", scene->cpu.cpu_gov);
	TLOGI("cpu_hot:%s", scene->cpu.cpu_hot);
	TLOGI("cpu_online:%s", scene->cpu.cpu_online);

	TLOGI("gpu_freq:%s", scene->gpu.gpu_freq);

	TLOGI("dram_adaptive:%s", scene->dram.dram_adaptive);
	TLOGI("dram_freq:%s", scene->dram.dram_freq);
	TLOGI("dram_freq_max:%s", scene->dram.dram_freq_max);
	TLOGI("dram_freq_min:%s", scene->dram.dram_freq_min);

	np_uci_close(uci);
	return ret;
}


#define PROP_SECTION		"properties"
#define PROP_OPTION_CUR_SCENE	"current_scene"

int np_get_cur_scene_name(char *scene_name, size_t len)
{
	np_get_property(PROP_SECTION, PROP_OPTION_CUR_SCENE, scene_name, len);
	return 0;
}

int np_set_cur_scene_name(const char *scene_name)
{
	return np_set_property(PROP_SECTION, PROP_OPTION_CUR_SCENE, scene_name);
}

int np_get_scene_property(const char *conf_name, char *conf_buf, size_t len)
{
	return np_get_property(PROP_SECTION, conf_name, conf_buf, len);
}

int np_set_scene_property(char *conf_name, char *conf_buf)
{
	return np_set_property(PROP_SECTION, conf_name, conf_buf);
}
