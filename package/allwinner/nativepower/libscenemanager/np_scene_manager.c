/*
 * Copyright (C) 2016 Allwinnertech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "np_sysfsop.h"
#include "np_scene_utils.h"


int np_scene_set(NP_SCENE *scene)
{
	CPU_SCENE_OPS   *cpu_ops  = np_get_sysfsops(CPU_INDEX);
	GPU_SCENE_OPS   *gpu_ops  = np_get_sysfsops(GPU_INDEX);
	DRAM_SCENE_OPS  *dram_ops = np_get_sysfsops(DRAM_INDEX);

	if (strlen(scene->cpu.bootlock) != 0)
		cpu_ops->SetBootLock(scene->cpu.bootlock);

	if (strlen(scene->cpu.roomage) != 0)
		cpu_ops->SetRoomAge(scene->cpu.roomage);

	if (strlen(scene->cpu.cpu_freq) != 0)
		cpu_ops->SetCpuFreq(scene->cpu.cpu_freq);

	if (strlen(scene->cpu.cpu_freq_max) != 0)
		cpu_ops->SetCpuFreqMax(scene->cpu.cpu_freq_max);

	if (strlen(scene->cpu.cpu_freq_min) != 0)
		cpu_ops->SetCpuFreqMin(scene->cpu.cpu_freq_min);

	if (strlen(scene->cpu.cpu_gov) != 0)
		cpu_ops->SetCpuGov(scene->cpu.cpu_gov);

	if (strlen(scene->cpu.cpu_hot) != 0)
		cpu_ops->SetCpuHot(scene->cpu.cpu_hot);

	if (strlen(scene->cpu.cpu_online) != 0)
		cpu_ops->SetCpuOnline(scene->cpu.cpu_online);

	if (strlen(scene->gpu.gpu_freq) != 0)
		gpu_ops->SetGpuFreq(scene->gpu.gpu_freq);

	if (strlen(scene->dram.dram_adaptive) != 0)
		dram_ops->SetDramFreqAdaptive(scene->dram.dram_adaptive);

	if (strlen(scene->dram.dram_freq) != 0)
		dram_ops->SetDramFreq(scene->dram.dram_freq);

	if (strlen(scene->dram.dram_freq_max) != 0)
		dram_ops->SetDramFreqMax(scene->dram.dram_freq_max);

	if (strlen(scene->dram.dram_freq_min) != 0)
		dram_ops->SetDramFreqMin(scene->dram.dram_freq_min);

	return 0;
}


int np_scene_change(const char *scene_name)
{
	NP_SCENE scene;

	memset(&scene, 0, sizeof(NP_SCENE));

	if (np_get_scene_config(&scene, scene_name) < 0)
		return -1;

	np_set_cur_scene_name(scene_name);
	np_scene_set(&scene);
	return 0;
}



int np_cpu_freq_get(char *freq, size_t len)
{
	CPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(CPU_INDEX);
	if (!ops || !ops->GetCpuFreq)
		return -1;
	return ops->GetCpuFreq(freq, len);
}

int np_cpu_freq_set(const char *freq)
{
	CPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(CPU_INDEX);
	if (!ops || !ops->SetCpuFreq)
		return -1;
	return ops->SetCpuFreq(freq);
}

int np_cpu_gov_get(char *gov, size_t len)
{
	CPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(CPU_INDEX);
	if (!ops || !ops->GetCpuGov)
		return -1;
	return ops->GetCpuGov(gov, len);
}

int np_cpu_online_get(char *online, size_t len)
{
	CPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(CPU_INDEX);
	if (!ops || !ops->GetCpuOnline)
		return -1;
	return ops->GetCpuOnline(online, len);
}

int np_dram_freq_get(char *freq, size_t len)
{
	DRAM_SCENE_OPS *ops;

	ops = np_get_sysfsops(GPU_INDEX);
	if (!ops || !ops->GetDramFreq)
		return -1;
	return ops->GetDramFreq(freq, len);
}

int np_dram_freq_set(const char *freq)
{
	DRAM_SCENE_OPS *ops;

	ops = np_get_sysfsops(GPU_INDEX);
	if (!ops || !ops->SetDramFreq)
		return -1;
	return ops->SetDramFreq(freq);
}

int np_gpu_freq_get(char *freq, size_t len)
{
	GPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(DRAM_INDEX);
	if (!ops || !ops->GetGpuFreq)
		return -1;
	return ops->GetGpuFreq(freq, len);
}

int np_gpu_freq_set(const char *freq)
{
	GPU_SCENE_OPS *ops;

	ops = np_get_sysfsops(DRAM_INDEX);
	if (!ops || !ops->SetGpuFreq)
		return -1;
	return ops->SetGpuFreq(freq);
}
